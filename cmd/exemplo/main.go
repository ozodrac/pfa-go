package main

import (
	"fmt"
	"sync"
)

func contador(T string) {
	for i := 0; i < 3; i++ {
		fmt.Println(T, i)
	}
}

func main() {
	wg := sync.WaitGroup{}
	wg.Add(1)
	func() {
		defer wg.Done()
		contador("a")
	}()
	wg.Wait()

	wg.Add(2)
	go func() {
		contador("b")
		wg.Done()
	}()
	go func() {
		contador("c")
		wg.Done()
	}()
	wg.Wait()
}
